/* Create a session for the connection */
var session = new QiSession();

var fName = null;
var currentHint = null;

var waitingPage = null;
var focusNode = null;

var memory = null;
var backgroundMovement = null;
var behaviourManager = null;
var basicAwareness = null;
var tabletService = null;
var speechService = null;
var tts = null;
var sp = null;

function startSubscribe() {
	introStr = document.getElementById('introSpeech').textContent;
	//buttons = document.getElementsByClassName('largeButton');
	if (!session) {
		//alert('No connection to a robot.');
		sayText(introStr);
		return;
	}
	session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("ALAnimatedSpeech/EndOfAnimatedSpeech").done(function(subscriber) {
            subscriber.signal.connect(endSpeech);
        });   
		memory = ALMemory;
    });
	session.service('ALBehaviorManager').then(function (service) {
		behaviourManager = service;
	});
	session.service("ALTextToSpeech").done(function (service) {
		speechService = service;
		speechService.setParameter("speed",85);
		session.service("ALAnimatedSpeech").done(function (service) {
			tts = service;
			tts.say(introStr);
		});
	});
	session.service("ALTabletService").done(function (service) {
		tabletService = service;
    });
	session.service("ALBasicAwareness").done(function (service) {
		basicAwareness = service;
		// Ensure basicAwareness is on
		basicAwareness.setEnabled(true);
		// Stop Pepper looking down at tablet every time you touch it!
		basicAwareness.setStimulusDetectionEnabled("TabletTouch", false);
    });
	session.service("ALBackgroundMovement").done(function (service) {
		backgroundMovement = service;
    });
}
function stopSubscribe() {
	if (session) {
		tabletService.resetTablet();
	}
}

function sayText(response) {
	if (tts) {
		tts.say(response);
	} else {
		/*
		if (sp) {
			var msg = new SpeechSynthesisUtterance(response);
			sp.speak(msg);
		} else {
			alert("can't speak");
		}
		*/
		alert("Pepper says: "+response);
		var msg = new SpeechSynthesisUtterance(response);
		window.speechSynthesis.speak(msg);		
		endSpeech();
	}
}

function endSpeech() {
	document.getElementById('firstButton').style.visibility = 'visible';
	if (waitingPage && waitingPage.style.display=='none') {
		waitingPage.style.display = 'flex';
		//focusNode.focus();
	} 
}

function interrupt_and_show(elementID) {
	if (speechService) {
		speechService.stopAll();
	}
	show(elementID);
}

function quitTopic() {
	document.getElementById('confirmQuit').style.display='block';
	if (speechService) {
		speechService.stopAll();
	}
}
function quitSession() {
	stopApp();
}
function resumeSession() {
	document.getElementById('confirmQuit').style.display='none';
}

function show(elementID) {	
	//alert("Size: " + window.innerWidth + " x " + window.innerHeight);
	// set background movement back on
	if (backgroundMovement) {
		backgroundMovement.setEnabled(true);
	}
	
    // try to find the requested page and alert if it's not found
    var ele = document.getElementById(elementID);
    if (!ele) {
        alert("no such page");
        return;
    }
	waitingPage = ele; // In case we have to wait for speech to finish

    // get all pages, loop through them and hide them
    var pages = document.getElementsByClassName('page');
	var pageCount = pages.length;
	var pageNum = 0;
    for(var i = 0; i < pageCount; i++) {
        pages[i].style.display = 'none';
		if (ele == pages[i]) {
			pageNum = i;
		}
    }
	var progressBar = document.getElementById("progress");
    if (progressBar) {
        //alert("Progress = "+parseInt(i)+" out of "+parsInt(pageCount));
		progressBar.style.visibility = "hidden";
		var progress = parseInt(pageNum*100/pageCount);
		styleStr = progress+"vw";
		//alert(styleStr);
		progressBar.style.width = styleStr;
		progressBar.style.visibility = "visible";
    }
				
	// find child elements
	var speaking = false;
	var children = ele.childNodes;
	for(var i = 0; i < children.length; i++) {
		var className = children[i].className;
        if (className == "aloud") {
			speaking = true;
			sayText(children[i].innerText);
		} else if (className == "userInput") {
			// Turn off the background movement, so it's easier to make input
			if (backgroundMovement) {
				backgroundMovement.setEnabled(false);
			}
			focusNode = children[i];
		} else if (className == "hint") {
			currentHint = children[i];
		}
    }
	
	// Finally show the requested page, unless waiting for speaking
    if (!speaking) {
		waitingPage.style.display = 'flex';
		focusNode.focus();
	}
}

function setName(nextPage) {
	var ele = document.getElementById("firstNameText");
        if (!ele) {
            alert("no element");
        } else {
			fName = ele.value;
		};
	if (fName.length == 0) {	
		recordData('firstName',fName);
		show(nextPage);
	} else if (dirtyWords(fName,badNames)) {
		fName = '';
		recordData('firstName',fName);
		show(nextPage);
	} else {
		recordData('firstName',fName);
		var nameTags = document.getElementsByClassName('firstName');
		for(var i = 0; i < nameTags.length; i++) {
			nameTags[i].innerText = fName;
		};
		show(nextPage);
	}
}

function dirtyWords(inputStr,badList) {
	//alert(inputStr);
	wordList = inputStr.split(' ');
	//alert(wordList);
	for (var index = 0; index < wordList.length; ++index) {
		word = wordList[index].toLowerCase();
		//alert(word)
		if (badList.indexOf(word) >= 0) {
			//alert("BAD WORD! "+badList[badList.indexOf(word)]);
			return true;
		}
	}
	return false;
}
	
function checkText(strInput,sayEmpty,sayDirty) {
	if (strInput.length == 0) {
		sayText(sayEmpty);
		return false;
	} else if (dirtyWords(strInput,badWords)) {
		sayText(sayDirty);
		return false;
	} else {
		return true;
	}
}

function showHint(set) {
	if (!currentHint) {
		alert("no current hint");
		return;
	}
	if (set) {
		currentHint.style.display='block';
	} else {
		currentHint.style.display='none';
	}
}

function recordFeedback(name1,name2,nextPage) {
	var resp1 = -1;
	var options = document.getElementsByName(name1);
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			resp1=i;
			break;
		}
	}
	var resp2 = -1;
	options = document.getElementsByName(name2);
	for(var i=0; options[i]; ++i){
		if(options[i].checked){
			resp2=i;
			break;
		}
	}
	if (resp1 < 0 || resp2 <0) {
		sayText("Please answer both questions before we go on.");
	} else {
		recordData(name1,resp1.toString());
		recordData(name2,resp2.toString());
		show(nextPage);
	}
}

function stopApp() {
	if (memory) {
		memory.raiseEvent("stopLog","done");
	} else {
		alert('No memory');
	}
	/*
	pathName = window.location.pathname;
	path = pathName.slice(0,pathName.lastIndexOf('/'));
	behaviourName = path.replace("/apps/", "")+"/behavior_1";
	behaviourManager.stopBehavior(behaviourName);
	*/
}

function checkEmail(nextPage) {
	var emailAddr = document.getElementById("emailTxt").value;
	var sendPlan = document.getElementById("sendPlanCheck").checked;
	//alert(sendPlan);
	if (emailAddr.length == 0) {
		if (sendPlan) {
			sayText("Oops. I will need your email address to send you a copy of your plan.");
		} else {
			sayText("Thats OK");
			show(nextPage);
		}
	} else {
		var ea = emailAddr.trim();
		if (ea.indexOf('@') == -1 || ea.indexOf('.') == -1) {
			sayText("Oh dear. That doesn't look right. Please check your email address and try again.");
		} else {
			recordData("sendPlan", sendPlan.toString());
			recordData("emailAddress",ea);
			show(nextPage);			
		}
	}
}

/*
function startLog() {
	randomfileName = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	//alert(randomfileName);
	memory.raiseEvent("startLog", randomfileName);
}
function recordData(nameStr,valStr) {
	if (nameStr == "emailAddress") {
		//valStr = Tea.encrypt(valStr, randomfileName);
		//alert("email encrypted: "+valStr);
		memory.raiseEvent("stopLog", valStr.trim());
		return;
	}
	var rec = '"'+valStr.trim()+'"';
	memory.raiseEvent("writeData", rec);
}
*/
function recordData(valStr) {
	var rec = '"'+valStr.trim()+'"';
	if (memory) {
		memory.raiseEvent("writeData", rec);
	}
}

function setTopic(topicName) {
	topic = topicName;
	score = 0;
}
	
function recordResp(nameStr,nextPage) {
	//sayText(nameStr);
	//sayText(nextPage);
	var options = document.getElementsByName(nameStr);
	var resp = "";
	for (var i=0; options[i]; ++i){
		//sayText(i.toString());
		if(options[i].checked){
			//sayText("checked");
			//checkedItem = options[i].previousSibling;
			//resp = checkedItem.textContent;
			if (nameStr == "gender") {
				checkedValue = options[i].value;
				recordData(checkedValue);
				//alert("gender: "+checkedValue);
			} else if (nameStr == "age") {
				checkedValue = options[i].value;
				recordData(checkedValue);
				//alert("age: "+checkedValue);
			} else {
				checkedValue = parseInt(options[i].value);
				recordData(options[i].value);
				score = score + checkedValue;			
				if (topic == "anxiety" ) {
					if (nameStr == "Q2" && checkedValue==1) {
						nextPage = "Q4";
						recordData("");
						score = score + 1; 
					}
					if (nameStr == "Q5" && checkedValue==1) {
						nextPage = "Q7";
						recordData("");
						score = score + 1; 
					}
				}
			}
			show(nextPage);
			return;
		}
	}
	/* if (resp.length > 0) {
		show(nextPage);
	} else { */
	sayText("Please choose a response before we go on.");
}
function calculateScore() {
	//alert('Topic: '+topic);
	//alert('Score: '+score.toString());
	//sayText(score.toString());
	var scoreTags = document.getElementsByClassName('scoreCalc');
	for(var i = 0; i < scoreTags.length; i++) {
		scoreTags[i].innerText = score.toString();
	};
	if (topic == "anxiety") {
		if (score<16) {
			show("lowScore");
		} else if (score<30) {
			show("moderateScore");
		} else {
			show("veryhighScore");
		};
	} else if (topic == "stress") {
		if (score<14) {
			show("lowScore");
		} else if (score<27) {
			show("moderateScore");
		} else {
			show("veryhighScore");
		};
	} else if (topic == "gambling") {
		if (score==0) {
			show("nonproblem");
		} else if (score<3) {
			show("lowRisk");
		} else if (score<8) {
			show("moderateRisk");
		} else {
			show("problem");
		};
	} else if (topic == "alcohol") {
		if (score<8) {
			show("zone1");
		} else if (score<16) {
			show("zone2");
		} else if (score<20) {
			show("zone3");
		} else {
			show("zone4");
		};
	} else if (topic == "anger") {
		if (score<9) {
			show("lowRisk");
		} else if (score<12) {
			show("moderateRisk");
		} else {
			show("problem");
		};
	} else if (topic == "ptsd") {
		if (score<32) {
			show("nonproblem");
		} else {
			show("problem");
		};
	};
}
