<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
        <message>
            <source>Oh dear. Something has gone wrong. I will need to rest a moment. Sorry.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh dear. Something has gone wrong. I will need to rest a moment. Sorry.</translation>
        </message>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Oh dear. Something has gone wrong. I will need to rest a moment. I'm Sorry.</source>
            <comment>Text</comment>
            <translation type="unfinished">Oh dear. Something has gone wrong. I will need to rest a moment. I'm Sorry.</translation>
        </message>
    </context>
</TS>
