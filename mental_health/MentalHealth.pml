<?xml version="1.0" encoding="UTF-8" ?>
<Package name="NursingEd" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="style" src="html/css/style.css" />
        <File name="icon" src="html/images/icon.png" />
        <File name="README" src="README.md" />
        <File name="calendar" src="html/css/calendar.css" />
        <File name="decrypt" src="html/decrypt.html" />
        <File name="delete" src="html/images/delete.png" />
        <File name="edit" src="html/images/edit.png" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="badnames" src="html/js/badnames.js" />
        <File name="badwords" src="html/js/badwords.js" />
        <File name="pureJSCalendar" src="html/js/pureJSCalendar.js" />
        <File name="head" src="head.png" />
        <File name="index" src="html/index.html" />
        <File name="Anxiety" src="html/Anxiety.html" />
        <File name="Gambling" src="html/Gambling.html" />
        <File name="index - Copy" src="html/index - Copy.html" />
        <File name="BLUEstyle" src="html/css/BLUEstyle.css" />
        <File name="Anger" src="html/Anger.html" />
        <File name="REDstyle" src="html/css/REDstyle.css" />
        <File name="Alcohol" src="html/Alcohol.html" />
        <File name="ORANGEstyle" src="html/css/ORANGEstyle.css" />
        <File name="drinks" src="html/drinks.html" />
        <File name="bottle-of-champagne_0" src="html/images/bottle-of-champagne_0.jpg" />
        <File name="bottle-of-full-strength-beer" src="html/images/bottle-of-full-strength-beer.jpg" />
        <File name="bottle-of-light-beer_0" src="html/images/bottle-of-light-beer_0.jpg" />
        <File name="bottle-of-mid-strength-beer_0" src="html/images/bottle-of-mid-strength-beer_0.jpg" />
        <File name="bottle-of-red-wine_2" src="html/images/bottle-of-red-wine_2.jpg" />
        <File name="bottle-of-spirits_0" src="html/images/bottle-of-spirits_0.jpg" />
        <File name="bottle-of-white-wine_0" src="html/images/bottle-of-white-wine_0.jpg" />
        <File name="can-of-full-strength-beer_0" src="html/images/can-of-full-strength-beer_0.jpg" />
        <File name="can-of-light-beer_0" src="html/images/can-of-light-beer_0.jpg" />
        <File name="can-of-mid-strength-beer" src="html/images/can-of-mid-strength-beer.jpg" />
        <File name="case-of-full-strength-beer_0" src="html/images/case-of-full-strength-beer_0.jpg" />
        <File name="case-of-light-beer_0" src="html/images/case-of-light-beer_0.jpg" />
        <File name="case-of-mid-strength-beer_0" src="html/images/case-of-mid-strength-beer_0.jpg" />
        <File name="cask-of-port_0" src="html/images/cask-of-port_0.jpg" />
        <File name="cask-of-red-wine-2-litre" src="html/images/cask-of-red-wine-2-litre.jpg" />
        <File name="cask-of-red-wine-4-litres_1" src="html/images/cask-of-red-wine-4-litres_1.jpg" />
        <File name="cask-of-white-wine-2-litres_0" src="html/images/cask-of-white-wine-2-litres_0.jpg" />
        <File name="cask-of-white-wine-4-litres" src="html/images/cask-of-white-wine-4-litres.jpg" />
        <File name="glass-of-champagne_0" src="html/images/glass-of-champagne_0.jpg" />
        <File name="glass-of-port_1" src="html/images/glass-of-port_1.jpg" />
        <File name="glass-of-red-wine-restaurant-size" src="html/images/glass-of-red-wine-restaurant-size.jpg" />
        <File name="glass-of-red-wine" src="html/images/glass-of-red-wine.jpg" />
        <File name="glass-of-spirits_0" src="html/images/glass-of-spirits_0.jpg" />
        <File name="glass-of-white-wine-restaurant" src="html/images/glass-of-white-wine-restaurant.jpg" />
        <File name="glass-of-white-wine" src="html/images/glass-of-white-wine.jpg" />
        <File name="large-bottle-of-full-strength-ready-to-drink-alcohol" src="html/images/large-bottle-of-full-strength-ready-to-drink-alcohol.jpg" />
        <File name="large-bottle-of-high-strength-ready-to-drink-alcohol" src="html/images/large-bottle-of-high-strength-ready-to-drink-alcohol.jpg" />
        <File name="large-can-of-full-strength-premix-spirits" src="html/images/large-can-of-full-strength-premix-spirits.jpg" />
        <File name="large-can-of-high-strength-premix-spirits_0" src="html/images/large-can-of-high-strength-premix-spirits_0.jpg" />
        <File name="large-glass-of-beer_1" src="html/images/large-glass-of-beer_1.jpg" />
        <File name="medium-bottle-of-full-strength-ready-to-drink-alcohol" src="html/images/medium-bottle-of-full-strength-ready-to-drink-alcohol.jpg" />
        <File name="medium-bottle-of-high-strength-ready-to-drink-alcohol" src="html/images/medium-bottle-of-high-strength-ready-to-drink-alcohol.jpg" />
        <File name="medium-can-of-full-strength-premix-spirits" src="html/images/medium-can-of-full-strength-premix-spirits.jpg" />
        <File name="medium-can-of-full-strength-premix-spirits_0" src="html/images/medium-can-of-full-strength-premix-spirits_0.jpg" />
        <File name="medium-can-of-high-strength-premix-spirits" src="html/images/medium-can-of-high-strength-premix-spirits.jpg" />
        <File name="medium-can-of-high-strength-premix-spirits_0" src="html/images/medium-can-of-high-strength-premix-spirits_0.jpg" />
        <File name="small-bottle-of-full-strength-ready-to-drink-alcohol" src="html/images/small-bottle-of-full-strength-ready-to-drink-alcohol.jpg" />
        <File name="small-bottle-of-high-strength-ready-to-drink-alcohol" src="html/images/small-bottle-of-high-strength-ready-to-drink-alcohol.jpg" />
        <File name="small-can-of-full-strength-premix-spirits" src="html/images/small-can-of-full-strength-premix-spirits.jpg" />
        <File name="small-can-of-high-strength-premix-spirits" src="html/images/small-can-of-high-strength-premix-spirits.jpg" />
        <File name="small-glass-of-beer_1" src="html/images/small-glass-of-beer_1.jpg" />
        <File name="AboutMe" src="html/AboutMe.html" />
        <File name="Introduction" src="html/Introduction.html" />
        <File name="introStyle" src="html/css/introStyle.css" />
        <File name="icon" src="icon.png" />
        <File name="bigTest" src="html/bigTest.html" />
        <File name="launcher-icon-4x" src="html/images/launcher-icon-4x.png" />
        <File name="manifest" src="html/manifest.json" />
        <File name="test" src="html/test.html" />
        <File name="PTSD" src="html/PTSD.html" />
        <File name="Stress" src="html/Stress.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
