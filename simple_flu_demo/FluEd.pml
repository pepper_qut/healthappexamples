<?xml version="1.0" encoding="UTF-8" ?>
<Package name="FluEd" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="style" src="html/css/style.css" />
        <File name="About" src="html/images/About.png" />
        <File name="BlueCircle" src="html/images/BlueCircle.png" />
        <File name="Cafe" src="html/images/Cafe.png" />
        <File name="Clock" src="html/images/Clock.png" />
        <File name="Directions" src="html/images/Directions.png" />
        <File name="Exit" src="html/images/Exit.png" />
        <File name="Feedback" src="html/images/Feedback.png" />
        <File name="Handwash" src="html/images/Handwash.png" />
        <File name="Parking" src="html/images/Parking.png" />
        <File name="RedSquare" src="html/images/RedSquare.png" />
        <File name="Rights" src="html/images/Rights.png" />
        <File name="Smoking" src="html/images/Smoking.png" />
        <File name="Worried" src="html/images/Worried.png" />
        <File name="index" src="html/index.html" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="choice_sentences_light" src="behavior_1/Aldebaran/choice_sentences_light.xml" />
        <File name="yesNo" src="html/yesNo.html" />
        <File name="Quiz1" src="html/Quiz1.html" />
        <File name="Cross" src="html/images/Cross.png" />
        <File name="Tick" src="html/images/Tick.png" />
        <File name="Vaccine" src="html/images/Vaccine.png" />
        <File name="icon" src="icon.png" />
        <File name="README" src="README.md" />
        <File name="quizAvoid" src="html/quizAvoid.html" />
        <File name="handwashQuiz" src="html/handwashQuiz.html" />
        <File name="AvoidPeople" src="html/images/AvoidPeople.png" />
        <File name="FaceMask" src="html/images/FaceMask.png" />
        <File name="gender" src="html/gender.html" />
        <File name="Female" src="html/images/Female.png" />
        <File name="Male" src="html/images/Male.png" />
        <File name="age" src="html/age.html" />
        <File name="CalculateAge" src="html/js/CalculateAge.js" />
        <File name="10sec" src="html/images/10sec.png" />
        <File name="20sec" src="html/images/20sec.png" />
        <File name="5sec" src="html/images/5sec.png" />
        <File name="60sec" src="html/images/60sec.png" />
        <File name="No" src="html/images/No.png" />
        <File name="Yes" src="html/images/Yes.png" />
        <File name="POC" src="html/POC.html" />
        <File name="AQLogo" src="html/images/AQLogo.PNG" />
        <File name="rv_logo_colour" src="html/images/rv_logo_colour.png" />
        <File name="QUTlogo" src="html/images/QUTlogo.png" />
        <File name="2button" src="html/2button.html" />
        <File name="POCscreen" src="html/POCscreen.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
