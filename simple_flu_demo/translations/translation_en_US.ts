<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Safe Say</name>
        <message>
            <source>Hello, I'm Pepper. I'd like to talk to you about the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello, I'm Pepper. I'd like to talk to you about the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (1)</name>
        <message>
            <source>That's good, because</source>
            <comment>Text</comment>
            <translation type="obsolete">That's good, because</translation>
        </message>
        <message>
            <source>That's good.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's good.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (10)</name>
        <message>
            <source>You should spend at least 20 seconds washing or rubbing your hands.</source>
            <comment>Text</comment>
            <translation type="obsolete">You should spend at least 20 seconds washing or rubbing your hands.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (11)</name>
        <message>
            <source>But remember. Even healthy people can catch the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">But remember. Even healthy people can catch the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (12)</name>
        <message>
            <source>Can you tell me how long you should spend washing or rubbing your hands to be effective.</source>
            <comment>Text</comment>
            <translation type="obsolete">Can you tell me how long you should spend washing or rubbing your hands to be effective.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (13)</name>
        <message>
            <source>That's not really enough.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's not really enough.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (14)</name>
        <message>
            <source>Great.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (15)</name>
        <message>
            <source>Oh. OK.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh. OK.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (16)</name>
        <message>
            <source>Thank you for stopping by for a chat.</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you for stopping by for a chat.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (17)</name>
        <message>
            <source>Do you think you'll get vaccinated this year?</source>
            <comment>Text</comment>
            <translation type="obsolete">Do you think you'll get vaccinated this year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (18)</name>
        <message>
            <source>First, can I ask: Did you have a flu vaccination last year?</source>
            <comment>Text</comment>
            <translation type="obsolete">First, can I ask: Did you have a flu vaccination last year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (19)</name>
        <message>
            <source>How long do you think the flu virus can live outside the human body?</source>
            <comment>Text</comment>
            <translation type="obsolete">How long do you think the flu virus can live outside the human body?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (2)</name>
        <message>
            <source>Oh.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (3)</name>
        <message>
            <source>Vaccination is the most effective way to protect yourself from getting the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">Vaccination is the most effective way to protect yourself from getting the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (4)</name>
        <message>
            <source>Well, actually, it's a few hours</source>
            <comment>Text</comment>
            <translation type="obsolete">Well, actually, it's a few hours</translation>
        </message>
        <message>
            <source>Well, actually, it's a few hours.</source>
            <comment>Text</comment>
            <translation type="obsolete">Well, actually, it's a few hours.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (5)</name>
        <message>
            <source>That's right.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's right.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (6)</name>
        <message>
            <source>That's why washing your hands is really important.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's why washing your hands is really important.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (7)</name>
        <message>
            <source>Soap and water is the best. Or you can use hand sanitiser.</source>
            <comment>Text</comment>
            <translation type="obsolete">Soap and water is the best. Or you can use hand sanitiser.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (8)</name>
        <message>
            <source>That's right.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's right.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Safe Say (9)</name>
        <message>
            <source>It doesn't even have to be that long.</source>
            <comment>Text</comment>
            <translation type="obsolete">It doesn't even have to be that long.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Thanks</name>
        <message>
            <source>Thank you for stopping by for a chat.</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you for stopping by for a chat.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Animated Say</name>
        <message>
            <source>Hello</source>
            <comment>Text</comment>
            <translation type="vanished">Hello</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Conclude</name>
        <message>
            <source>Vaccination is the most effective way to protect yourself from getting the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">Vaccination is the most effective way to protect yourself from getting the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Intro</name>
        <message>
            <source>Hello, I'm Pepper. I'd like to talk to you about the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello, I'm Pepper. I'd like to talk to you about the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/No Response</name>
        <message>
            <source>OK.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Question</name>
        <message>
            <source>First, can I ask: Did you have a flu vaccination last year?</source>
            <comment>Text</comment>
            <translation type="obsolete">First, can I ask: Did you have a flu vaccination last year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Safe Say</name>
        <message>
            <source>Hello, I'm Pepper. I'd like to talk to you about the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">Hello, I'm Pepper. I'd like to talk to you about the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Safe Say (1)</name>
        <message>
            <source>That's good.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's good.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Safe Say (2)</name>
        <message>
            <source>OK.</source>
            <comment>Text</comment>
            <translation type="obsolete">OK.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Safe Say (3)</name>
        <message>
            <source>First, can I ask: Did you have a flu vaccination last year?</source>
            <comment>Text</comment>
            <translation type="obsolete">First, can I ask: Did you have a flu vaccination last year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate last year/Yes Response</name>
        <message>
            <source>That's good.</source>
            <comment>Text</comment>
            <translation type="obsolete">That's good.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Conclude</name>
        <message>
            <source>Thank you for stopping by for a chat</source>
            <comment>Text</comment>
            <translation type="obsolete">Thank you for stopping by for a chat</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Intro</name>
        <message>
            <source>But remember. Even healthy people can catch the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">But remember. Even healthy people can catch the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/No Response</name>
        <message>
            <source>Oh. OK. Your choice.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh. OK. Your choice.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Question</name>
        <message>
            <source>Do you intend to get vaccinated this year?</source>
            <comment>Text</comment>
            <translation type="obsolete">Do you intend to get vaccinated this year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Safe Say</name>
        <message>
            <source>But remember. Even healthy people can catch the flu.</source>
            <comment>Text</comment>
            <translation type="obsolete">But remember. Even healthy people can catch the flu.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Safe Say (1)</name>
        <message>
            <source>Great.</source>
            <comment>Text</comment>
            <translation type="obsolete">Great.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Safe Say (2)</name>
        <message>
            <source>Oh. OK.</source>
            <comment>Text</comment>
            <translation type="obsolete">Oh. OK.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Safe Say (3)</name>
        <message>
            <source>Do you think you'll get vaccinated this year?</source>
            <comment>Text</comment>
            <translation type="obsolete">Do you think you'll get vaccinated this year?</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Vaccinate next year/Yes Response</name>
        <message>
            <source>Great</source>
            <comment>Text</comment>
            <translation type="obsolete">Great</translation>
        </message>
    </context>
</TS>
