<?xml version="1.0" encoding="UTF-8" ?>
<Package name="RUOK" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="Quiz1" src="html/Quiz1.html" />
        <File name="age" src="html/age.html" />
        <File name="continue" src="html/continue.html" />
        <File name="style" src="html/css/style.css" />
        <File name="gender" src="html/gender.html" />
        <File name="handwashQuiz" src="html/handwashQuiz.html" />
        <File name="10sec" src="html/images/10sec.png" />
        <File name="20sec" src="html/images/20sec.png" />
        <File name="5sec" src="html/images/5sec.png" />
        <File name="60sec" src="html/images/60sec.png" />
        <File name="About" src="html/images/About.png" />
        <File name="AvoidPeople" src="html/images/AvoidPeople.png" />
        <File name="Clock" src="html/images/Clock.png" />
        <File name="Cross" src="html/images/Cross.png" />
        <File name="FaceMask" src="html/images/FaceMask.png" />
        <File name="Female" src="html/images/Female.png" />
        <File name="Handwash" src="html/images/Handwash.png" />
        <File name="Male" src="html/images/Male.png" />
        <File name="No" src="html/images/No.png" />
        <File name="RUOK_Logo" src="html/images/RUOK_Logo.png" />
        <File name="Tick" src="html/images/Tick.png" />
        <File name="Vaccine" src="html/images/Vaccine.png" />
        <File name="Yes" src="html/images/Yes.png" />
        <File name="index" src="html/index.html" />
        <File name="CalculateAge" src="html/js/CalculateAge.js" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="quizAvoid" src="html/quizAvoid.html" />
        <File name="role" src="html/role.html" />
        <File name="yesNo" src="html/yesNo.html" />
        <File name="icon" src="html/images/icon.png" />
        <File name="smiley" src="html/images/smiley.png" />
        <File name="icon" src="icon.png" />
        <File name="SomeoneTroubled" src="html/images/SomeoneTroubled.png" />
        <File name="1_ask" src="html/1_ask.html" />
        <File name="Ask" src="html/images/Ask.jpg" />
        <File name="troubled" src="html/images/troubled.png" />
        <File name="whoToAsk" src="html/images/whoToAsk.png" />
        <File name="2_listen" src="html/2_listen.html" />
        <File name="Listen" src="html/images/Listen.jpg" />
        <File name="Check" src="html/images/Check.jpg" />
        <File name="3_action" src="html/3_action.html" />
        <File name="4_check" src="html/4_check.html" />
        <File name="Action" src="html/images/Action.jpg" />
        <File name="intro" src="html/intro.html" />
        <File name="ask1" src="html/ask1.html" />
        <File name="Ask1" src="html/images/Ask1.png" />
        <File name="ruok-1" src="html/images/ruok-1.png" />
        <File name="Listen2" src="html/images/Listen2.png" />
        <File name="action3" src="html/action3.html" />
        <File name="Action3" src="html/images/Action3.png" />
        <File name="check4" src="html/check4.html" />
        <File name="Check4" src="html/images/Check4.png" />
        <File name="notReady" src="html/images/notReady.png" />
        <File name="notReady" src="html/notReady.html" />
        <File name="summary" src="html/images/summary.png" />
        <File name="listen2" src="html/listen2.html" />
        <File name="steps" src="html/steps.html" />
        <File name="finish" src="html/finish.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
