/****************************************************/
/* Aldebaran Behavior Complementary Development Kit */
/* ConnectionHtmlChoregraphe: Connection.js         */
/* Innovation - Protolab - mcaniot@aldebaran.com    */
/* Aldebaran Robotics (c) 2016 All Rights Reserved. */
/* This file is confidential.                       */
/* NOTE: THIS FILE IS PUBLISHED ONLINE               */
/****************************************************/

/* Create a session for the connection */
var session = new QiSession();

/* This function allow to connect with ALMemory thank to the box "Raise Event".
You need to give the key "PepperQiMessaging/totablet"*/
function startSubscribe() {
    session.service("ALMemory").done(function (ALMemory) {
        ALMemory.subscriber("PepperQiMessaging/totablet").done(function(subscriber) {
            subscriber.signal.connect(toTabletHandler);
        });    
    });
}

/* Receive the data send by choregraphe with the id "command". 
You can change the name of the id.*/ 
function toTabletHandler(value) { 
    // get the data and put it in the id "command"
    //document.getElementById("count").value= value;
    //tmp = document.getElementById("command").value;
    // send the data to html page
    document.getElementById("question").innerHTML = String(value);
}

/* Send information to choregraphe thank to the event "PepperQiMessaging/fromtablet".
You need to create this event in choregraphe (add event from ALMemory).*/
function sendToChoregraphe(response) {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/fromTabletResponse", response);
    });
}
function interruptSpeech() {
    session.service("ALMemory").done(function (ALMemory) {
        console.log("ALMemory");
        ALMemory.raiseEvent("PepperQiMessaging/interruptSpeech", 1);
    });
}
