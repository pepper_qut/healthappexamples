<?xml version="1.0" encoding="UTF-8" ?>
<Package name="handrub" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="Handrub" src="Handrub.png" />
        <File name="DontTouch" src="html/DontTouch.jpg" />
        <File name="Handrub1" src="html/Handrub1.jpg" />
        <File name="Handrub2" src="html/Handrub2.jpg" />
        <File name="Handrub3" src="html/Handrub3.jpg" />
        <File name="Handrub3B" src="html/Handrub3B.jpg" />
        <File name="Handrub4" src="html/Handrub4.jpg" />
        <File name="Handrub5" src="html/Handrub5.jpg" />
        <File name="Handrub5B" src="html/Handrub5B.jpg" />
        <File name="Handrub6" src="html/Handrub6.jpg" />
        <File name="Handrub7" src="html/Handrub7.jpg" />
        <File name="Handrub8" src="html/Handrub8.jpg" />
        <File name="HandrubScript" src="html/HandrubScript.txt" />
        <File name="icon" src="icon.png" />
        <File name="translation_en_US" src="translations/translation_en_US.qm" />
        <File name="README" src="README.md" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
