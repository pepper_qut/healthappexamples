<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
    <context>
        <name>behavior_1/behavior.xar:/Explain</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Hey. Did you know that regular hand washing is the best way to get rid of germs? And to protect yourself from disease.</source>
            <comment>Text</comment>
            <translation type="unfinished">Hey. Did you know that regular hand washing is the best way to get rid of germs? And to protect yourself from disease.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Explain (1)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Like the flu. E col I. And the common cold.</source>
            <comment>Text</comment>
            <translation type="unfinished">Like the flu. E col I. And the common cold.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Lets start</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>First, squeeze a palmful of the sanitiser into your hand. And spread it all over your hands.</source>
            <comment>Text</comment>
            <translation type="unfinished">First, squeeze a palmful of the sanitiser into your hand. And spread it all over your hands.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 1</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Then rub your hands together, palm to palm.</source>
            <comment>Text</comment>
            <translation type="unfinished">Then rub your hands together, palm to palm.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 2A</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Now put one hand over the back of your other hand, interlace your fingers, and rub to clean between your fingers.</source>
            <comment>Text</comment>
            <translation type="unfinished">Now put one hand over the back of your other hand, interlace your fingers, and rub to clean between your fingers.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 2B</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Swap the other hand to the top, and rub between your fingers on that hand as well.</source>
            <comment>Text</comment>
            <translation type="unfinished">Swap the other hand to the top, and rub between your fingers on that hand as well.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 3</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Palms back together, interlace your fingers to rub between them that way too.</source>
            <comment>Text</comment>
            <translation type="unfinished">Palms back together, interlace your fingers to rub between them that way too.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 5A</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Next, rub the back of your fingers on the opposite palm. Make sure you swap and do that on both hands.</source>
            <comment>Text</comment>
            <translation type="unfinished">Next, rub the back of your fingers on the opposite palm. Make sure you swap and do that on both hands.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 6</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Now clean each thumb. By wrapping your other hand around it. And rubbing. Do that for both thumbs.</source>
            <comment>Text</comment>
            <translation type="unfinished">Now clean each thumb. By wrapping your other hand around it. And rubbing. Do that for both thumbs.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 7</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Finally, rub your fingertips into your palm. Do that for each hand.</source>
            <comment>Text</comment>
            <translation type="unfinished">Finally, rub your fingertips into your palm. Do that for each hand.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 7 (1)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>By the time your hands are dry. You're safe.</source>
            <comment>Text</comment>
            <translation type="unfinished">By the time your hands are dry. You're safe.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Step 7 (2)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>I hope you have a wonderful day!</source>
            <comment>Text</comment>
            <translation type="unfinished">I hope you have a wonderful day!</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Washing</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>You should spend 20 to 30 seconds thoroughy washing or sanitising your hands.</source>
            <comment>Text</comment>
            <translation type="unfinished">You should spend 20 to 30 seconds thoroughy washing or sanitising your hands.</translation>
        </message>
    </context>
    <context>
        <name>behavior_1/behavior.xar:/Washing (1)</name>
        <message>
            <location filename="behavior_1/behavior.xar" line="0"/>
            <source>Come on. It's easy. I'll talk you through it.</source>
            <comment>Text</comment>
            <translation type="unfinished">Come on. It's easy. I'll talk you through it.</translation>
        </message>
    </context>
</TS>
