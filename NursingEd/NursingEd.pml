<?xml version="1.0" encoding="UTF-8" ?>
<Package name="NursingEd" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="style" src="html/css/style.css" />
        <File name="index" src="html/index.html" />
        <File name="BGL" src="html/images/BGL.png" />
        <File name="icon" src="html/images/icon.png" />
        <File name="icon" src="icon.png" />
        <File name="Connection" src="html/js/Connection.js" />
        <File name="index_old" src="html/index_old.html" />
        <File name="quizScript" src="html/quizScript.js" />
        <File name="IV" src="html/images/IV.png" />
        <File name="terminology" src="html/images/terminology.png" />
        <File name="medic" src="html/images/medic.png" />
        <File name="reactions" src="html/reactions.js" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
    </Translations>
</Package>
