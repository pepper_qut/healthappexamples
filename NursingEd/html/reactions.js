var postiveReactions = [
	"^start(animations/Stand/BodyTalk/Listening/Listening_1)",
	"^start(animations/Stand/BodyTalk/Listening/Listening_2)",
	"^start(animations/Stand/Emotions/Positive/Sure_1)",
	"^start(animations/Stand/Emotions/Positive/Confident_1)"
];
var negativeReactions = [
	"^start(animations/Stand/Gestures/DontUnderstand_1)",
	"^start(animations/Stand/Gestures/IDontKnow_1)",
	"^start(animations/Stand/Gestures/IDontKnow_2)",
	"^start(animations/Stand/Gestures/IDontKnow_3)",
	"^start(animations/Stand/Gestures/IDontKnow_4)",
	"^start(animations/Stand/Gestures/IDontKnow_5)",
	"^start(animations/Stand/Gestures/IDontKnow_6)"
];
